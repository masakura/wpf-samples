﻿using System.Collections.Generic;

namespace FlexibleApp
{
    public sealed class MainWindowViewModel
    {
        public MainWindowViewModel()
        {
            Customers = new FakerCustomer().Generate(30);
        }

        public IEnumerable<Customer> Customers { get; }
    }
}