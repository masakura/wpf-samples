﻿using Bogus;

namespace FlexibleApp
{
    public sealed class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public bool NeedAttention { get; set; }
    }
}