﻿using Bogus;

namespace FlexibleApp
{
    sealed class FakerCustomer : Faker<Customer>
    {
        public FakerCustomer() : base("ja")
        {
            RuleFor(o => o.FirstName, f => f.Name.FirstName());
            RuleFor(o => o.LastName, f => f.Name.LastName());
            RuleFor(o => o.Address, f => f.Address.FullAddress());
            RuleFor(o => o.NeedAttention, f => f.Random.Bool());
        }
    }
}