﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using ThemeApp.Annotations;

namespace ThemeApp
{
    public sealed class MainWindowViewModel : INotifyPropertyChanged
    {
        private Theme _selectedTheme;

        public MainWindowViewModel()
        {
            var themes = Theme.All();

            Themes = themes;
            SelectedTheme = themes.First();
        }

        public IEnumerable<Theme> Themes { get; }

        public Theme SelectedTheme
        {
            get => _selectedTheme;
            set
            {
                if (Equals(value, _selectedTheme)) return;
                _selectedTheme = value;

                value.Apply();

                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}