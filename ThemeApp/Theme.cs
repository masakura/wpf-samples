﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace ThemeApp
{
    public sealed class Theme
    {
        private readonly string _displayName;
        private readonly string _name;

        public Theme(string name, string displayName)
        {
            _name = name;
            _displayName = displayName;
        }

        public void Apply()
        {
            Application.Current.Resources.Clear();

            var resources = new ResourceDictionary
            {
                Source = new Uri($"pack://application:,,,/themes/{_name}.xaml", UriKind.RelativeOrAbsolute)
            };
            Application.Current.Resources.MergedDictionaries.Add(resources);
        }

        public override string ToString()
        {
            return _displayName;
        }

        public static IEnumerable<Theme> All()
        {
            return new[]
            {
                new Theme("default", "WPF Default"),
                new Theme("red", "Red"),
                new Theme("green", "Green"),
                new Theme("blue", "Blue")
            };
        }
    }
}