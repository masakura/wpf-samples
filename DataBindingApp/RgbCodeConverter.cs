﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DataBindingApp
{
    public sealed class RgbCodeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = value as RgbColor;

            return color?.AsHtmlColorCode();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (RgbColor.TryParse($"{value}", out var code)) return code;

            return DependencyProperty.UnsetValue;
        }
    }
}