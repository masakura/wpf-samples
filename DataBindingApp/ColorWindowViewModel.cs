﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using DataBindingApp.Annotations;

namespace DataBindingApp
{
    public sealed class ColorWindowViewModel : INotifyPropertyChanged
    {
        private RgbColor _color;

        public ColorWindowViewModel()
        {
            Color = new RgbColor();
        }

        public RgbColor Color
        {
            get => _color;
            set
            {
                if (Equals(value, _color)) return;
                Monitor(_color, value);
                _color = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Monitor(RgbColor old, RgbColor @new)
        {
            if (@new != null) @new.PropertyChanged += FirePropertyChanged;
            if (old != null) old.PropertyChanged -= FirePropertyChanged;
        }

        private void FirePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(nameof(Color));
        }
    }
}