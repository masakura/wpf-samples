﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows.Media;
using DataBindingApp.Annotations;

namespace DataBindingApp
{
    public sealed class RgbColor : INotifyPropertyChanged
    {
        private byte _blue;
        private byte _green;
        private byte _red;

        public byte Red
        {
            get => _red;
            set
            {
                if (value == _red) return;
                _red = value;
                OnPropertyChanged();
            }
        }

        public byte Blue
        {
            get => _blue;
            set
            {
                if (value == _blue) return;
                _blue = value;
                OnPropertyChanged();
            }
        }

        public byte Green
        {
            get => _green;
            set
            {
                if (value == _green) return;
                _green = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public SolidColorBrush ToSolidBrush()
        {
            return new SolidColorBrush(ToColor());
        }

        private Color ToColor()
        {
            return Color.FromRgb(Red, Green, Blue);
        }

        public string AsHtmlColorCode()
        {
            return $"#{Red:X2}{Green:X2}{Blue:X2}";
        }

        public static bool TryParse(string s, out RgbColor value)
        {
            var match = Regex.Match((s ?? "").ToLowerInvariant(),
                "^(#|)([0-9a-f][0-9a-f])([0-9a-f][0-9a-f])([0-9a-f][0-9a-f])$");
            if (!match.Success)
            {
                value = null;
                return false;
            }

            value = new RgbColor
            {
                Red = Convert.ToByte(match.Groups[2].Value, 16),
                Green = Convert.ToByte(match.Groups[3].Value, 16),
                Blue = Convert.ToByte(match.Groups[4].Value, 16)
            };

            return true;
        }
    }
}