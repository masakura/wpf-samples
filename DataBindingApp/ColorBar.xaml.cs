﻿using System.Windows;

namespace DataBindingApp
{
    /// <summary>
    ///     ColorBar.xaml の相互作用ロジック
    /// </summary>
    public partial class ColorBar
    {
        public static readonly DependencyProperty ColorProperty = DependencyProperty.Register(
            "Color", typeof(byte), typeof(ColorBar), new PropertyMetadata(default(byte)));

        public ColorBar()
        {
            InitializeComponent();
        }

        public byte Color
        {
            get => (byte) GetValue(ColorProperty);
            set => SetValue(ColorProperty, value);
        }
    }
}