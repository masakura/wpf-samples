﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace DataBindingApp
{
    public sealed class BrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Brush brush) return brush;

            var color = (RgbColor) value;
            return color?.ToSolidBrush();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}