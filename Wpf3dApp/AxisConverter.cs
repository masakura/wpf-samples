using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Wpf3dApp
{
    public sealed class AxisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is AxisViewModel axis)) return DependencyProperty.UnsetValue;

            return axis.ToVector3D();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}