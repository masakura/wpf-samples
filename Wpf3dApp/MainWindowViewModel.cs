﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Wpf3dApp.Annotations;

namespace Wpf3dApp
{
    public sealed class MainWindowViewModel : INotifyPropertyChanged
    {
        private AxisViewModel _axis;

        public MainWindowViewModel()
        {
            Axis = new AxisViewModel();
        }

        public AxisViewModel Axis
        {
            get => _axis;
            set
            {
                if (Equals(value, _axis)) return;
                if (_axis != null) _axis.PropertyChanged -= AxisOnPropertyChanged;
                if (value != null) value.PropertyChanged += AxisOnPropertyChanged;

                _axis = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void AxisOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(nameof(Axis));
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}