﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Media3D;
using Wpf3dApp.Annotations;

namespace Wpf3dApp
{
    public sealed class AxisViewModel : INotifyPropertyChanged
    {
        private double _x;
        private double _y;
        private double _z;

        public double X
        {
            get => _x;
            set
            {
                if (value.Equals(_x)) return;
                _x = value;
                OnPropertyChanged();
            }
        }

        public double Y
        {
            get => _y;
            set
            {
                if (value.Equals(_y)) return;
                _y = value;
                OnPropertyChanged();
            }
        }

        public double Z
        {
            get => _z;
            set
            {
                if (value.Equals(_z)) return;
                _z = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Vector3D ToVector3D()
        {
            return new Vector3D(X, Y, Z);
        }
    }
}